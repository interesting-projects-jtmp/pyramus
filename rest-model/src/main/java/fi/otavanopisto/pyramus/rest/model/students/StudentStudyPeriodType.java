package fi.otavanopisto.pyramus.rest.model.students;

public enum StudentStudyPeriodType {

  TEMPORARILY_SUSPENDED,
  PROLONGED_STUDYENDDATE
  
}
