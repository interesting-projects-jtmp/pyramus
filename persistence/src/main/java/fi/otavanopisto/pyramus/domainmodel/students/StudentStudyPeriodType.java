package fi.otavanopisto.pyramus.domainmodel.students;

public enum StudentStudyPeriodType {

  TEMPORARILY_SUSPENDED,
  PROLONGED_STUDYENDDATE
  
}
